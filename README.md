# CURP Generator
Aplicación Android que permite generar la Clave Única de Registro de Población (véase https://es.wikipedia.org/wiki/Clave_%C3%9Anica_de_Registro_de_Poblaci%C3%B3n) de México.

# Autor
Jose Miguel García Ramos 2018

# Licencia
MIT License

