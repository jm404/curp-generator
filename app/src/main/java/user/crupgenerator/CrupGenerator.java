package user.crupgenerator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.view.ViewGroup;
import android.graphics.Color;
import java.util.Arrays;

public class CrupGenerator extends AppCompatActivity {

    public boolean esVocal(Character texto){
        if (texto == 'a' || texto == 'e' || texto == 'i' || texto == 'o' || texto == 'u'
                || texto == 'A' || texto == 'E' || texto == 'I' || texto == 'O' || texto == 'U'
                || texto == 'Á' || texto == 'É' || texto == 'Í' || texto == 'Ó' || texto == 'Ú'
                || texto == 'á' || texto == 'é' || texto == 'í' || texto == 'ó' || texto == 'ú'
                || texto == '/' || texto == '-' ){
            return true;
        }else{
            return false;
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crup_generator);



        // Spinner del Sexo

            // Obteniendo referencia

                final Spinner spinnerSexos = (Spinner) findViewById(R.id.spinnerSexo);

            // Inicializando array de sexos
                String[] arraySexos = {"Elige tu sexo","Hombre", "Mujer"};
                final List<String> listaSexos = new ArrayList<>(Arrays.asList(arraySexos));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerSexosArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,listaSexos){

            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {

                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerSexosArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerSexos.setAdapter(spinnerSexosArrayAdapter);

        spinnerSexos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if(position > 0){

                    Toast.makeText
                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner del estado

        // Obteniendo referencia

            final Spinner spinnerEstados = (Spinner) findViewById(R.id.spinnerEstados);

        // Inicializando array de sexos

        String[] arrayEstados = {"Elige tu estado",
                "Extranjero",
                "Aguascalientes",
                "Baja California",
                "Baja California Sur",
                "Campeche",
                "Chiapas",
                "Chihuahua",
                "Coahuila",
                "Colima",
                "Ciudad de México",
                "Durango",
                "Guanajuato",
                "Guerrero",
                "Hidalgo",
                "Jalisco",
                "Estado de México",
                "Michoacán",
                "Morelos",
                "Nayarit",
                "Nuevo León",
                "Oaxaca",
                "Puebla",
                "Querétaro",
                "Quintana Roo",
                "San Luis Potosi",
                "Sinaloa",
                "Sonora",
                "Tabasco",
                "Tamaulipas",
                "Tlaxcala",
                "Veracruz",
                "Yucatán ",
                "Zacatecas"};


        final List<String> listaEstados = new ArrayList<>(Arrays.asList(arrayEstados));


        // Initicializando arrayAdapter de Estados
        final ArrayAdapter<String> spinnerEstadosArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,listaEstados){

            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {

                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerEstadosArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerEstados.setAdapter(spinnerEstadosArrayAdapter);

        spinnerEstados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);

                if(position > 0){

                    Toast.makeText
                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button clickButton = (Button) findViewById(R.id.buttonGenerarCURP);
        clickButton.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick(View v) {



                // Obteniendo texto de los textviews

                EditText nombreEditText = (EditText) findViewById(R.id.editText);
                String nombreTexto = nombreEditText.getText().toString().toUpperCase();

                nombreTexto = nombreTexto.replaceAll("\\bMARIA\\s\\b","");
                nombreTexto = nombreTexto.replaceAll("\\bJOSE\\s\\b","");


                EditText primerApellidoEditText = (EditText) findViewById(R.id.editText2);
                String primerApellidoTexto = primerApellidoEditText.getText().toString().toUpperCase();

                EditText segundoApellidoEditText = (EditText) findViewById(R.id.editText5);
                String segundoApellidoTexto = segundoApellidoEditText.getText().toString().toUpperCase();

                EditText fechaNacimientoEditText = (EditText) findViewById(R.id.editText8);
                String fechaNacimientoTexto = fechaNacimientoEditText.getText().toString().toUpperCase();

                String fechaNacimientoTextotest = fechaNacimientoTexto;

                String sexoTexto = spinnerSexos.getSelectedItem().toString();
                String estadoTexto = spinnerEstados.getSelectedItem().toString();

                // Lista Resultado

                List<Character> listaResultado = new ArrayList<>();

                // String Resultado

                String resultado = "";

                // Verificando datos

                boolean datosCorrectos = true;

                if (nombreTexto.isEmpty() || primerApellidoTexto.isEmpty() || fechaNacimientoTexto.isEmpty()
                        || fechaNacimientoTexto.length() < 6 || sexoTexto.isEmpty() || estadoTexto.isEmpty()){
                    datosCorrectos = false;
                    resultado = "ERROR. Datos incorrectos";

                }

                if (datosCorrectos){

                    // Convirtiendo textos a Listas

                    List<Character> nombreLista = new ArrayList<>();
                    for (int i=0; i< nombreTexto.length();i++){
                        nombreLista.add(nombreTexto.charAt(i));
                    }

                    List<Character> primerApellidoLista = new ArrayList<>();
                    for (int i=0; i< primerApellidoTexto.length();i++){
                        primerApellidoLista.add(primerApellidoTexto.charAt(i));
                    }

                    List<Character> segundoApellidoLista = new ArrayList<>();
                    for (int i=0; i< segundoApellidoTexto.length();i++){
                        segundoApellidoLista.add(segundoApellidoTexto.charAt(i));
                    }

                    // Calculando 4 primeros caracteres

                    boolean segundaLetraConsonante = false;
                    boolean noSegundoApellido = false;

                    // ¿Consonante segunda letra?
                    if (!(esVocal(primerApellidoLista.get(1)))){
                        segundaLetraConsonante = true;
                    }

                    if (segundoApellidoTexto == ""){
                        noSegundoApellido = true;
                    }

                    // Lista Resultado

                    //primera letra: si no segunda componante -> dos primeras letras = dos primeras letras 1er apellido

                    int posicionPrimeraVocal = 0;

                    if (segundaLetraConsonante == false){
                        listaResultado.add(primerApellidoLista.get(0));
                        listaResultado.add(primerApellidoLista.get(1));
                    }else{
                        boolean romperIf = false;
                        for (int i=0; i < primerApellidoLista.size();i++){
                            if(esVocal(primerApellidoLista.get(i)) && romperIf == false){
                                listaResultado.add(primerApellidoLista.get(i));
                                posicionPrimeraVocal = i;
                                romperIf = true;
                            }
                        }
                    }

                    //segunda letra: buscamos la primera vocal interna del apellido

                    if (listaResultado.size() == 1){
                        for (int i=posicionPrimeraVocal+1 ; i < primerApellidoLista.size(); i++){
                            if(esVocal(primerApellidoLista.get(i)) && listaResultado.size() < 2){
                                listaResultado.add(primerApellidoLista.get(i));
                            }
                        }
                    }

                    if(listaResultado.size()==1){
                        listaResultado.add('X');
                    }

                    // tercera letra: inicial del segundo apellido

                    if (segundoApellidoLista.isEmpty() == true){
                        listaResultado.add('X');
                    }else{
                        listaResultado.add(segundoApellidoLista.get(0));
                    }

                    // cuarta letra: inicial del nombre

                    listaResultado.add(nombreLista.get(0));

                    // Fecha de nacimiento

                    fechaNacimientoTextotest = fechaNacimientoTextotest.replaceAll("-","");
                    fechaNacimientoTextotest = fechaNacimientoTextotest.replaceAll("/","");

                    for (int i=0;i<fechaNacimientoTextotest.length();i++){
                        listaResultado.add(fechaNacimientoTextotest.charAt(i));
                    }

                    // Sexo

                    listaResultado.add(sexoTexto.charAt(0));

                    // Estado

                    String codigoEstado = "";

                    switch (estadoTexto){
                        case "Extranjero": codigoEstado="NE"; break;
                        case "Aguascalientes": codigoEstado="AS" ; break;
                        case "Baja California": codigoEstado="BC" ; break;
                        case "Baja California Sur": codigoEstado="BS" ; break;
                        case "Campeche": codigoEstado="CC" ; break;
                        case "Chiapas": codigoEstado="CS" ; break;
                        case "Chihuahua": codigoEstado="CH" ; break;
                        case "Coahuila": codigoEstado="CL" ; break;
                        case "Colima": codigoEstado="CM" ; break;
                        case "Ciudad de México": codigoEstado="DF" ; break;
                        case "Durango": codigoEstado="DG" ; break;
                        case "Guanajuato": codigoEstado="GT" ; break;
                        case "Guerrero": codigoEstado="GR" ; break;
                        case "Hidalgo": codigoEstado="HG" ; break;
                        case "Jalisco": codigoEstado="JC" ; break;
                        case "Estado de México": codigoEstado="MC" ; break;
                        case "Michoacán": codigoEstado="MN" ; break;
                        case "Morelos": codigoEstado="MS" ; break;
                        case "Nayarit": codigoEstado="NT" ; break;
                        case "Nuevo León": codigoEstado="NL" ; break;
                        case "Oaxaca": codigoEstado="OC" ; break;
                        case "Puebla": codigoEstado="PL" ; break;
                        case "Querétaro": codigoEstado="QO" ; break;
                        case "Quintana Roo": codigoEstado="QR" ; break;
                        case "San Luis Potosi": codigoEstado="SP" ; break;
                        case "Sinaloa": codigoEstado="SL" ; break;
                        case "Sonora": codigoEstado="SR" ; break;
                        case "Tabasco": codigoEstado="TC" ; break;
                        case "Tamaulipas": codigoEstado="TS" ; break;
                        case "Tlaxcala": codigoEstado="TL" ; break;
                        case "Veracruz": codigoEstado="VZ" ; break;
                        case "Yucatán ": codigoEstado="YN" ; break;
                        case "Zacatecas": codigoEstado="ZS" ; break;

                    }

                    listaResultado.add(codigoEstado.charAt(0));
                    listaResultado.add(codigoEstado.charAt(1));

                    // Consonantes internas de nombre y apellidos

                    // Consonante interna primer apellido

                    for(int i=1; i < primerApellidoLista.size() ; i++){
                        if(esVocal(primerApellidoLista.get(i)) == false){
                            listaResultado.add(primerApellidoLista.get(i));
                            break;
                        }
                    }

                    if(listaResultado.size() < 14){
                        listaResultado.add('X');
                    }

                    // Consonante interna segundo apellido

                    for(int i=1; i < segundoApellidoLista.size() ; i++){
                        if(segundoApellidoLista.isEmpty()){
                            listaResultado.add('X');
                            break;
                        }

                        if(esVocal(segundoApellidoLista.get(i)) == false){
                            listaResultado.add(segundoApellidoLista.get(i));
                            break;
                        }
                    }

                    if (listaResultado.size() < 15){
                        listaResultado.add('X');
                    }

                    // Consonante interna nombre

                    for(int i=1; i < nombreLista.size() ; i++){
                        if(esVocal(nombreLista.get(i)) == false){
                            listaResultado.add(nombreLista.get(i));
                            break;
                        }
                    }

                    if (listaResultado.size()<16){
                        listaResultado.add('X');
                    }


                    StringBuilder builder = new StringBuilder(listaResultado.size());
                    for(Character ch: listaResultado)
                    {
                        builder.append(ch);
                    }

                    String listaResultadoTexto = builder.toString();

                    listaResultadoTexto = listaResultadoTexto.replaceAll("Ñ","X");
                    listaResultadoTexto = listaResultadoTexto.replaceAll("\\-","X");
                    listaResultadoTexto = listaResultadoTexto.replaceAll("\\.", "X");
                    listaResultadoTexto = listaResultadoTexto.replace('Ä','A')
                            .replace('Ë','E')
                            .replace('Ï','I')
                            .replace('Ö','O')
                            .replace('Ü','U');
                    listaResultadoTexto = listaResultadoTexto.replaceAll(" ","");

                    resultado = listaResultadoTexto;

                }

                AlertDialog alertDialog = new AlertDialog.Builder(CrupGenerator.this).create();
                alertDialog.setTitle("Codigo CURP: ");
                alertDialog.setMessage(resultado);

                // Boton Dialogo

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Alerta del dialogo
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

                TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                textView.setTextIsSelectable(true);
                textView.setTextSize(23);



            }
        });

    }

}
